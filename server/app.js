var express = require('express');
var path = require('path');
var app = express();

const NODE_PORT = 8080;
// define paths
const CLIENT_FOLDER = path.join(__dirname , '/../client');
const MSG_FOLDER = path.join(CLIENT_FOLDER, '/assets/messages');
//static content
app.use(express.static(CLIENT_FOLDER));
// ERROR HANDLERS 
// Handles resource not found it is a catch all
app.use(function(req, res) {
  res
    .status(404)
    .sendFile(path.join(MSG_FOLDER, '/404.html'));
});
// Handles server error
app.use(function(err, req, res, next) {
  console.log(err);
  res
    .status(500)
    .sendFile(path.join(MSG_FOLDER, "/500.html"));
});

app.listen(NODE_PORT, function() {
    console.log("Welcome to port: " + NODE_PORT);
});